I created this simple page to help family and friends to find where to use the [CDC vouchers](https://vouchers.cdc.gov.sg/). 

<img src="screenshot/0.png" width="240">
<img src="screenshot/1.png" width="240">

You may consider using the official [CDC Go Where](https://www.gowhere.gov.sg/cdcvouchersmerchants/map) site as an alternate.

This service is provided on a goodwill basis with no implied warranty for your convenience. Feel free to leave a feedback if you have any.